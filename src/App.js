import logo from './logo.svg';
import './App.css';
import react from 'react'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import DataTable from './table'
import React from 'react';
import styles from './myStyles.css'

function App(){
  return(
    <React.Fragment>
      <h1 className="title">React app using hooks to display the data in a table</h1>
      <DataTable />
    </React.Fragment>
  );
}

export default App;