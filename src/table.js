import React, { useState, useEffect } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.css';
import paginationFactory from "react-bootstrap-table2-paginator";
import axios from 'axios'
import './App.css';
import {BeatLoader} from 'react-spinners'

function DataList() {
  const [loadingData, setLoadingData] = useState(true);
  const [data, setData] = useState([]);
  const pagination = paginationFactory({
    page: 1,
    sizePerPage: 5,
    lastPageText: '>>',
    firstPageText: '<<',
    nextPageText: '>',
    prePageText: '<',
    showTotal: true,
    alwaysShowAllBtns: true,
    onPageChange: function (page, sizePerPage) {
    console.log('page', page);
    console.log('sizePerPage', sizePerPage);
    },
    onSizePerPageChange: function (page, sizePerPage) {
    console.log('page', page);
    console.log('sizePerPage', sizePerPage);
    }
    });

  useEffect(() => {
    async function getData() {
      await axios
        .get("https://api.punkapi.com/v2/beers?page=1&per_page=10")
        .then((response) => {
       
          console.log(response.data);
          setData(response.data);
          
          setLoadingData(false);
        })
        .catch(error => alert(error));
    }
    if (loadingData) {
      
      getData();
    }
  }, []);

  

  const columns = [
    {
      dataField: "id",
      text: "ID",
      sort: true
    },
    {
      dataField: "name",
      text: "NAME"
    },
    {
      dataField: "tagline",
      text: "TAGLINE",
      // filter: textFilter()
    },
    {
      dataField: "first_brewed",
      text: "FIRST BREWED"
    },
   
  ]



return (
  <div>  <BootstrapTable  striped
  hover keyField='id' columns={columns} data={data} pagination={pagination} /></div>
);
}
export default DataList;